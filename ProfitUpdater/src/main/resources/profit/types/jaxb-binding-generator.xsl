<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml file:///D:/GIT/Dedicon/pib/broker/src/main/resources/profit/types/FbItemArticle%20pretty.xml?>
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:jxb="http://java.sun.com/xml/ns/jaxb">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:strip-space elements="*"/>
	
		<xsl:template match="/">
		<jxb:bindings xmlns:jxb="http://java.sun.com/xml/ns/jaxb" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<xsl:for-each select="//xsd:element[not(matches(@name, '[A-Z0-9]{33}')) and xsd:annotation/xsd:documentation]">
				  <jxb:bindings node="//xsd:element[@name='{@name}']">
					  <jxb:property name="{@name}">
						  <jxb:javadoc><xsl:value-of select="xsd:annotation/xsd:documentation"/></jxb:javadoc>
					  </jxb:property>      
				  </jxb:bindings>
			</xsl:for-each>
			<xsl:for-each select="//xsd:element[matches(@name, '[A-Z0-9]{33}') and xsd:annotation/xsd:documentation]">
				  <jxb:bindings node="//xsd:element[@name='{@name}']">
					  <jxb:property name="{substring-before(concat(string-join(xsd:annotation/xsd:documentation, ' '), ' '), ' ')}">
						  <jxb:javadoc><xsl:value-of select="xsd:annotation/xsd:documentation"/></jxb:javadoc>
					  </jxb:property>      
				  </jxb:bindings>
			</xsl:for-each>
			</jxb:bindings>
		</xsl:template>
	
</xsl:transform>
