<?xml version="1.0" encoding="UTF-8" ?>
<!-- This stylesheet transforms adjacents comments in a XSD to proper documentation/annotation nodes -->
<!-- See also: http://stackoverflow.com/questions/25377448/transform-adjacent-comments-into-xsannotations-in-xsds -->
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml" encoding="UTF-8" indent="yes" />
    <xsl:strip-space elements="*"/>    

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="xsd:element">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:variable name="com" select="preceding-sibling::comment() except preceding-sibling::*[local-name() = 'element'][1]/preceding-sibling::comment()"/>
            <xsl:if test="$com">
                <xsd:annotation>
                    <xsl:for-each select="$com">
                        <xsd:documentation>
                            <xsl:value-of select="normalize-space(.)"/>
                        </xsd:documentation>
                    </xsl:for-each>
                </xsd:annotation>
            </xsl:if>

            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="comment()"/>

</xsl:transform>