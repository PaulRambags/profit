package nl.dedicon.profitupdater.client;

import java.math.BigInteger;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLHandshakeException;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import nl.afas.profit.services.ConnectorAppGet;
import nl.afas.profit.services.ConnectorAppGetSoap;
import nl.afas.profit.services.ConnectorAppUpdate;
import nl.afas.profit.services.ConnectorAppUpdateSoap;
import nl.afas.profit.services.filter.Filters;
import nl.afas.profit.services.options.Options;
import nl.afas.profit.services.options.Outputmode;
import nl.dedicon.profitupdater.helper.XmlHelper;
import org.apache.commons.lang3.StringUtils;

/**
 * Client for the Profit SOAP web service
 * 
 * @author Martin Devillers
 */
public class ProfitClient {
    
    // Profit configuration
    private static final String token = "<token><version>1</version><data>6A07A0AB97A446399AF1D5391FA1936B954097C4492B58E6EEA68882F645E84E</data></token>";

    private static final String getConnectorEndpointUrl = "https://79995.afasonlineconnector.nl/profitservices/appconnectorget.asmx";
    private static final int getConnectorRequestTimeout = 30000;
    private static final int getConnectorConnectTimeout = 3000;
    private static final int getConnectorConnectMaxRetries = 7;
    private static final String updateConnectorEndpointUrl = "https://79995.afasonlineconnector.nl/profitservices/appconnectorupdate.asmx";
    private static final int updateConnectorRequestTimeout = 60000;
    private static final int updateConnectorConnectTimeout = 3000;

    private static final String GET_CONNECTOR_ID_DEDICON_ARTIKELEN = "Dedicon_Artikelen";
    private static final String GET_CONNECTOR_ID_DEDICON_PRODUCTVARIANTEN = "Dedicon_ProductVarianten";
    private static final String GET_CONNECTOR_ID_DEDICON_REPRODUCTIES = "Dedicon_Reproducties";
    private static final String GET_CONNECTOR_ID_DEDICON_NIEUWEREPRODUCTIES = "Dedicon_NieuweReproducties";
    private static final String GET_CONNECTOR_ID_DEDICON_GECONVERTEERDEREPRODUCTIES = "Dedicon_GeconverteerdeReproducties";
    private static final String GET_CONNECTOR_ID_DEDICON_ACTIEVEREPRODUCTIES = "Dedicon_ActieveReproducties";
    private static final String GET_CONNECTOR_ID_DEDICON_GELEVERDEREPRODUCTIES = "Dedicon_GeleverdeReproducties";
    private static final String GET_CONNECTOR_ID_DEDICON_VERZONDENREPRODUCTIES = "Dedicon_VerzondenReproducties";
    private static final String GET_CONNECTOR_ID_DEDICON_RESETREPRODUCTIES = "Dedicon_ResetReproducties";
    private static final String GET_CONNECTOR_ID_DEDICON_VERKOOPRELATIES = "Dedicon_VerkoopRelaties";
    private static final String GET_CONNECTOR_ID_DEDICON_VOORLEZERS = "Dedicon_Voorlezers";
    private static final String GET_CONNECTOR_ID_DEDICON_PROJECTEN = "Dedicon_Projecten";
    private static final String GET_CONNECTOR_ID_DEDICON_DOSSIERS = "Dedicon_Dossiers";
    private static final String GET_CONNECTOR_ID_DEDICON_TEVERPLAATSENPRODUCTEN = "Dedicon_TeVerplaatsenProducten";
    private static final String GET_CONNECTOR_ID_DEDICON_TECONVERTERENPRODUCTEN = "Dedicon_TeConverterenProducten";
    private static final String GET_CONNECTOR_ID_DEDICON_TEARCHIVERENPRODUCTEN = "Dedicon_TeArchiverenProducten";
    private static final String GET_CONNECTOR_ID_DEDICON_TEUITLEENKLAARZETTENPRODUCTEN = "Dedicon_TeUitleenKlaarzettenProducten";
    private static final String GET_CONNECTOR_ID_DEDICON_AFGERONDELEESSESSIES = "Dedicon_AfgerondeLeessessies";
    private static final String GET_CONNECTOR_ID_DEDICON_PRODUCTIEWORKFLOWS = "Dedicon_ProductieWorkflows";
    private static final String GET_CONNECTOR_ID_DEDICON_TEMETADATERENPRODUCTEN = "Dedicon_TeMetadaterenProducten";
    private static final String GET_CONNECTOR_ID_DEDICON_PERIODIEKEN = "Dedicon_Periodieken";
    private static final String GET_CONNECTOR_ID_DEDICON_EDITIES = "Dedicon_Edities";
    private static final String GET_CONNECTOR_ID_DEDICON_ACTIEVELEESSESSIES_DOSSIERS = "Dedicon_ActieveLeesSessies_Dossiers";
    private static final String GET_CONNECTOR_ID_DEDICON_ACTIEVELEESSESSIES_PROJECTEN = "Dedicon_ActieveLeesSessies_Projecten";
    private static final String GET_CONNECTOR_ID_DEDICON_LICENSELETTER = "Dedicon_LicenseLetter";

    
    public List<nl.afas.profit.services.dedicon_periodieken.AfasGetConnector.DediconPeriodieken> getPeriodieken() {
        
        System.out.println("Getting periodicals from Profit");
        
        String resultXml = tryGetData(GET_CONNECTOR_ID_DEDICON_PERIODIEKEN, 0, 1000);
        
        return XmlHelper.unmarshal(resultXml, nl.afas.profit.services.dedicon_periodieken.AfasGetConnector.class).getDediconPeriodieken();
    }
    
    public <T> String updateItem(T item) {
        
        System.out.println(String.format("Updating item %s in Profit", item.getClass().getSimpleName()));
        
        ConnectorAppUpdate service = new ConnectorAppUpdate();
        ConnectorAppUpdateSoap proxy = service.getConnectorAppUpdateSoap();
        BindingProvider bp = (BindingProvider)proxy;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, updateConnectorEndpointUrl);
        bp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, updateConnectorRequestTimeout);
        bp.getRequestContext().put(BindingProviderProperties.CONNECT_TIMEOUT, updateConnectorConnectTimeout);
        
        String connectorId = item.getClass().getSimpleName();
        
        // Ugly hack due to a naming decrepency in Profit
        if("KnWorkflow".equals(connectorId)) {
            connectorId = "KnSubjectWorkflowReaction";
        }
        String dataXml = XmlHelper.marshalStrict(item);
        System.out.println(dataXml);
        
        try {
            return proxy.execute(token, connectorId, 1, dataXml);
        } catch(WebServiceException ex) {
            if(!isIntermittentError(ex)) {
                System.out.println(String.format("ERROR: Profit UpdateConnector %s rejected message %s with error %s", connectorId, dataXml, ex.getMessage()));
            }
            throw ex;
        }
    }
    
    private String tryGetData(String connectorId, int skip, int take) {
        Options options = new Options();
        options.setOutputmode(Outputmode.Xml);
        options.setMetadata(BigInteger.ZERO);
        options.setTake(BigInteger.valueOf(take));
        options.setSkip(BigInteger.valueOf(skip));
        
        return tryGetData(connectorId, skip, take, (Filters)null);
    }
    
    private String tryGetData(String connectorId, int skip, int take, Filters filters) {
        Options options = new Options();
        options.setOutputmode(Outputmode.Xml);
        options.setMetadata(BigInteger.ZERO);
        options.setTake(BigInteger.valueOf(take));
        options.setSkip(BigInteger.valueOf(skip));
        
        return tryGetData(connectorId, skip, take, filters, options);
    }
    
    private String tryGetData(String connectorId, int skip, int take, Options options) {
        return tryGetData(connectorId, skip, take, null, options);
    }
    
    private String tryGetData(String connectorId, int skip, int take, Filters filters, Options options) {
        return tryGetData(connectorId, skip, take, filters, options, 0);
    }
    
    private String tryGetData(String connectorId, int skip, int take, Filters filters, Options options, Integer retryCount) {
        
        String filtersXml = null;
        if(filters != null) {
            filtersXml = XmlHelper.marshalStrict(filters);
            System.out.println("filtersXml: " + filtersXml);
        }
        
        String optionsXml = XmlHelper.marshalStrict(options);
        System.out.println("optionsXml: " + optionsXml);
        
        ConnectorAppGetSoap proxy = createGetConnectorProxy();
        try {
            String resultXml = proxy.getDataWithOptions(token, connectorId, filtersXml, skip, take, optionsXml);
            System.out.println("resultXml: " + resultXml);
            return resultXml;
        } catch (WebServiceException ex) {
            if(isIntermittentError(ex)) {
                if(retryCount == Math.min(getConnectorConnectMaxRetries, 10)) {
                    System.out.println("ERROR: Max retries achieved for intermittent error from Profit");
                    throw ex;
                } else {
                    System.out.println("WARING: Failed to get data from Profit due to an intermittent error (e.g. network connectivity). Attempt " + retryCount);
                    try {
                        TimeUnit.SECONDS.sleep(Double.valueOf(Math.pow(2, retryCount)).longValue());
                    } catch (InterruptedException ex1) {
                        System.out.println("WARNING: Profit.GetData retry-thread interrupted " + ex1);
                    }
                    return tryGetData(connectorId, skip, take, filters, options, ++retryCount); 
                }
            } else {
                throw ex;
            }
        }
    }
    
    public Boolean isIntermittentError(WebServiceException error)
    {
        return error.getCause() instanceof SocketTimeoutException 
            || error.getCause() instanceof ConnectException 
            || error.getCause() instanceof SSLHandshakeException 
            || StringUtils.containsAny(error.getMessage(), 
                "Inloggegevens niet gevonden", 
                "Deze omgeving kan niet worden geopend",
                "ActiveX component can't create object",
                "(AntaObject) Data is changed by an other user",
                "because it is being used by another process",
                "Het systeem heeft een ongewoon aantal foutieve inlogpogingen gedetecteerd",
                "Invalid procedure call or argument",
                "Unexpected backend error",
                "(Anta Data Access) A deadlock occured in the database.")
        ;
    }
    
    private ConnectorAppGetSoap createGetConnectorProxy() {
        ConnectorAppGet service = new ConnectorAppGet();
        ConnectorAppGetSoap proxy = service.getConnectorAppGetSoap();
        BindingProvider bp = (BindingProvider)proxy;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, getConnectorEndpointUrl);
        bp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, getConnectorRequestTimeout);
        bp.getRequestContext().put(BindingProviderProperties.CONNECT_TIMEOUT, getConnectorConnectTimeout);
        return proxy;
    }
    
}
