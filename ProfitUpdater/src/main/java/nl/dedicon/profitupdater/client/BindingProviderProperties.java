package nl.dedicon.profitupdater.client;

/**
 * Copy of com.sun.xml.internal.ws.client.BindingProviderProperties since we're
 * not allowed to use com.sun.* packages..
 * 
 * @author Martin Devillers
 */
public final class BindingProviderProperties {
    public static final java.lang.String CONNECT_TIMEOUT = "com.sun.xml.internal.ws.connect.timeout";
    public static final java.lang.String REQUEST_TIMEOUT = "com.sun.xml.internal.ws.request.timeout";
}
