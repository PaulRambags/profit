package nl.dedicon.profitupdater.model;

/**
 *
 * @author Martin Devillers
 */
public interface Address {
    /**
     * Gets the value of the debtorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebtorId();

    /**
     * Sets the value of the debtorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebtorId(String value);

    /**
     * Gets the value of the organisatiePersoonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganisatiePersoonCode();

    /**
     * Sets the value of the organisatiePersoonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganisatiePersoonCode(String value);

    /**
     * Gets the value of the postbusadres property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPostbusadres();

    /**
     * Sets the value of the postbusadres property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPostbusadres(Boolean value);

    /**
     * Gets the value of the straat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStraat();

    /**
     * Sets the value of the straat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStraat(String value);

    /**
     * Gets the value of the huisnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getHuisnummer();

    /**
     * Sets the value of the huisnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setHuisnummer(Long value);

    /**
     * Gets the value of the toevAanHuisnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToevAanHuisnr();

    /**
     * Sets the value of the toevAanHuisnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToevAanHuisnr(String value);

    /**
     * Gets the value of the postcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostcode();

    /**
     * Sets the value of the postcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostcode(String value);

    /**
     * Gets the value of the woonplaats property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWoonplaats();

    /**
     * Sets the value of the woonplaats property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWoonplaats(String value);

    /**
     * Gets the value of the land property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLand();

    /**
     * Sets the value of the land property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLand(String value);
}
