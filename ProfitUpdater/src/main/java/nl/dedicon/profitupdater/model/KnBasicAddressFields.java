package nl.dedicon.profitupdater.model;

import java.time.LocalDate;
import javax.xml.bind.JAXBElement;

/**
 *
 * @author Martin Devillers
 */
public interface KnBasicAddressFields {

    /**
     * Gets the value of the coId property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCoId();

    /**
     * Sets the value of the coId property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCoId(String value);

    /**
     * Gets the value of the pbAd property.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isPbAd();

    /**
     * Sets the value of the pbAd property.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public void setPbAd(Boolean value);

    /**
     * Gets the value of the stAd property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getStAd();

    /**
     * Sets the value of the stAd property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setStAd(JAXBElement<String> value);

    /**
     * Gets the value of the ad property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getAd();

    /**
     * Sets the value of the ad property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setAd(JAXBElement<String> value);

    /**
     * Gets the value of the hmNr property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link Long }{@code >}
     *
     */
    public JAXBElement<Long> getHmNr();

    /**
     * Sets the value of the hmNr property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link Long }{@code >}
     *
     */
    public void setHmNr(JAXBElement<Long> value);

    /**
     * Gets the value of the hmAd property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getHmAd();

    /**
     * Sets the value of the hmAd property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setHmAd(JAXBElement<String> value);

    /**
     * Gets the value of the zpCd property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getZpCd();

    /**
     * Sets the value of the zpCd property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setZpCd(JAXBElement<String> value);

    /**
     * Gets the value of the rs property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getRs();

    /**
     * Sets the value of the rs property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setRs(JAXBElement<String> value);

    /**
     * Gets the value of the adAd property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getAdAd();

    /**
     * Sets the value of the adAd property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setAdAd(JAXBElement<String> value);

    /**
     * Gets the value of the beginDate property.
     *
     * @return possible object is {@link String }
     *
     */
    public LocalDate getBeginDate();

    /**
     * Sets the value of the beginDate property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setBeginDate(LocalDate value);

    /**
     * Gets the value of the resZip property.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isResZip();

    /**
     * Sets the value of the resZip property.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public void setResZip(Boolean value);
}
