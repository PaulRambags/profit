package nl.dedicon.profitupdater;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import nl.afas.profit.services.dedicon_periodieken.AfasGetConnector.DediconPeriodieken;
import nl.afas.profit.types.kncustomk01.AFASRowAction;
import nl.afas.profit.types.kncustomk01.KnCustomK01;
import nl.afas.profit.types.kncustomk01.KnCustomK01.Element.Fields;
import nl.afas.profit.types.kncustomk01.ObjectFactory;
import nl.dedicon.profitupdater.client.ProfitClient;

public class Main {

    private static final ProfitClient PROFIT_CLIENT = new ProfitClient();
    private static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
    private static final Map<String,String> mapping;    // periodiekGroep code -> productGroep code
    private static final String defaultMapping;
    
    public static void main(String[] args)  {
        
        List<DediconPeriodieken> periodieken = PROFIT_CLIENT.getPeriodieken();
        KnCustomK01 update = new KnCustomK01();
        periodieken.stream()
                .map(Main::map)
                .filter(Objects::nonNull)
                .forEach(update.getElement()::add);
        if (update.getElement().isEmpty()) {
            System.out.println("Nothing to do");
        } else {
            System.out.println(String.format("Updating %d items in Profit", update.getElement().size()));
            PROFIT_CLIENT.updateItem(update);
        }
    }
    
    public static KnCustomK01.Element map(DediconPeriodieken periodiek) {
        String perCode = periodiek.getPercode();
        String productgroepCode = periodiek.getDediconProductgroepCode();
        if (!(productgroepCode == null || productgroepCode.isEmpty())) {
            System.out.println(String.format("Periodiek %s, existing productgroep with code %s left unchanged", perCode, productgroepCode));
            return null;
        }
        
        KnCustomK01.Element element = new KnCustomK01.Element();
        Fields fields = new Fields();
        fields.setAction(AFASRowAction.UPDATE);
        fields.setAuNu(OBJECT_FACTORY.createKnCustomK01ElementFieldsAuNu(false));
        fields.setSqNo(OBJECT_FACTORY.createKnCustomK01ElementFieldsSqNo(periodiek.getVolgnummer()));

        String periodiekgroepCode = periodiek.getDediconPeriodiekgroepCode();
        productgroepCode = mapping.get(periodiekgroepCode);
        if (productgroepCode == null) {
            productgroepCode = defaultMapping;
        }
        fields.setU96CB8C624A58929B3BA3329AFE542029(OBJECT_FACTORY.createKnCustomK01ElementFieldsU96CB8C624A58929B3BA3329AFE542029(productgroepCode));

        element.setFields(fields);
        return element;
    }
    
    static {
        defaultMapping = "OV";
        mapping = new HashMap<>();
        mapping.put("PG01", "KR");
        mapping.put("PG02", "KR");
        mapping.put("PG03", "KR");
        mapping.put("PG05", "JN");
        mapping.put("PG10", "TS");
        mapping.put("PG11", "TS");
        mapping.put("PG12", "TS");
        mapping.put("PG13", "TS");
        mapping.put("PG14", "TS");
        mapping.put("PG15", "TS");
        mapping.put("PG16", "TS");
        mapping.put("PG17", "TS");
        mapping.put("PG18", "TS");
        mapping.put("PG19", "TS");
        mapping.put("PG20", "TS");
        mapping.put("PG21", "TS");
        mapping.put("PG22", "TS");
        mapping.put("PG23", "TS");
        mapping.put("PG24", "TS");
        mapping.put("PG25", "TS");
        mapping.put("PG26", "TS");
        mapping.put("PG27", "TS");
        mapping.put("PG28", "TS");
        mapping.put("PG29", "TS");
        mapping.put("PG30", "TS");
        mapping.put("PG31", "TS");
        mapping.put("PG40", "HS");
        mapping.put("PG45", "HC");
        mapping.put("PG50", "FB");
        mapping.put("PG51", "FB");
        mapping.put("PG52", "FB");
        mapping.put("PG53", "FB");
        mapping.put("PG60", "JN");
        mapping.put("PG70", "SV");
        mapping.put("PG75", "HL");
        mapping.put("PG80", "OV");
        mapping.put("PG90", "OV");
    }
}
