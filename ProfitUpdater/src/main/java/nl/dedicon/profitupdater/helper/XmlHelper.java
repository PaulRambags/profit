package nl.dedicon.profitupdater.helper;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.concurrent.ConcurrentHashMap;
import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;


/**
 * Utility class for handling XML-data
 * 
 * @author Martin Devillers
 */
public class XmlHelper {
    
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(XmlHelper.class);
    private static ConcurrentHashMap<Class,JAXBContext> contextCache = new ConcurrentHashMap<>();
    
    /**
     * Marshals the object to a XML-based string using the default settings of JAXB.
     * These are "best-effort" settings, which are fine for logging, testing or 
     * debugging but should not be used for actual production application.
     * 
     * @param object The object to be marshaled
     * @return The XML-representation of the object
     */
    public static String marshalDefault(Object object) {
        StringWriter writer = new StringWriter();
        JAXB.marshal(object, writer);
        return writer.toString();
    }
    
    /**
     * Marshals the object to a XML-based string using the strict settings of JAXB.
     * 
     * @param <T> The type of the object
     * @param object The object to be marshaled
     * @return The XML-representation of the object
     */
    public static <T> String marshalStrict(T object) {
        StringWriter writer = new StringWriter();
        try {
            Class clazz = object instanceof JAXBElement ? ((JAXBElement<T>)object).getDeclaredType() : object.getClass();
            JAXBContext context;
            if(contextCache.containsKey(clazz)) {
                context = contextCache.get(clazz);
            } else {
                context = JAXBContext.newInstance(clazz);   
                contextCache.putIfAbsent(clazz, context);
            }
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-16");
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "");
            marshaller.marshal(object, writer);
        } catch (JAXBException ex) {
            logger.error("Error while marshalling object to XML", ex);
            throw new RuntimeException(ex);
        }
        return StringUtils.replaceOnce(writer.toString(), "xsi:schemaLocation=\"\"", "");
    }
    
    /**
     * Unmarshals a XML-string to a typed object
     * 
     * @param <T> The type of the object
     * @param xml The XML-representation of the object
     * @param clazz The class of the object
     * @return Returns the typed object
     */
    public static <T> T unmarshal(String xml, Class<T> clazz) {
        StringReader reader = new StringReader(xml);
        return JAXB.unmarshal(reader, clazz);
    }
}
