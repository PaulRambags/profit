package nl.dedicon.profitupdater.helper;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Scanner;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

/**
 * Provides various exception-free parsing and formatting methods,
 * that can be used for non-strict mappers.
 * 
 * @author Martin Devillers
 */
public class Try {
    
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Try.class);
    
    public static Long parseLong(String string) {
        try {
            return new Scanner(string).useDelimiter("\\D+").nextLong();
        } catch(Throwable t) {
            logger.warn("Failed to parse long", t);
            return null;
        }
    }

    public static Long parseLongNull(String string) {
        return string == null ? null : parseLong(string);
    }
    
    public static Long parseLong(Integer integer) {
        try {
            return Long.valueOf(integer);
        } catch(Throwable t) {
            logger.warn("Failed to parse long", t);
            return null;
        }
    }
    
    public static Long parseLongNull(Integer integer) {
        return integer == null ? null : parseLong(integer);
    }

    public static Long parseLong(BigInteger bigInteger) {
        try {
            return bigInteger.longValue();
        } catch(Throwable t) {
            logger.warn("Failed to parse long", t);
            return null;
        }
    }
    
    public static Long parseLongNull(BigInteger bigInteger) {
        return bigInteger == null ? null : parseLong(bigInteger);
    }
    
    public static BigDecimal parseBigDecimal(String string)
    {
        try {
            return new BigDecimal(string);
        } catch(Throwable t) {
            logger.warn("Failed to parse big decimal", t);
            return null;
        }
    }
    
    public static BigDecimal parseBigDecimalNull(String string) {
        return string == null ? null : parseBigDecimal(string);
    }
    
    public static BigInteger parseBigInteger(Long longg) {
        try {
            return BigInteger.valueOf(longg);
        } catch(Throwable t) {
            logger.warn("Failed to parse big integer", t);
            return null;
        }
    }
    
    public static BigInteger parseBigIntegerNull(Long longg) {
        return longg == null ? null : parseBigInteger(longg);
    }
    
    public static BigInteger parseBigInteger(String string) {
        return parseBigInteger(parseLong(string));
    }
    
    public static BigInteger parseBigIntegerNull(String string) {
        return string == null ? null : parseBigInteger(string);
    }
    
    public static Integer parseInteger(Long longg) {
        try {
            return longg.intValue();
        } catch(Throwable t) {
            logger.warn("Failed to parse integer", t);
            return null;
        }
    }

    public static Integer parseIntegerNull(Long longg) {
        return longg == null ? null : parseInteger(longg);
    }
    
    public static Integer parseInteger(String string) {
        try {
            return Integer.parseInt(string);
        } catch(Throwable t) {
            logger.warn("Failed to parse integer", t);
            return null;
        }
    }
    
    public static Integer parseIntegerNull(String string) {
        return string == null ? null : parseInteger(string);
    }

    public static String format(Long longg) {
        try {
            return longg.toString();
        } catch(Throwable t) {
            logger.warn("Failed to format long", t);
            return null;
        }
    }
    
    public static String formatNull(Long longg) {
        return longg == null ? null : format(longg);
    }
    
    public static String format(BigInteger bigInteger) {
        try {
            return bigInteger.toString();
        } catch(Throwable t) {
            logger.warn("Failed to format big integer", t);
            return null;
        }
    }
    
    public static String formatNull(BigInteger bigInteger) {
        return bigInteger == null ? null : format(bigInteger);
    }

    public static String truncate(String str, Integer maxWidth) {
        String result = StringUtils.truncate(str, maxWidth);
        if(!StringUtils.equals(str, result)) {
            logger.warn("Lost data while truncating '{}' to '{}'", str, result);
        }
        return result;
    }
}
